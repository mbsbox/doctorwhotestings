using Sandbox;

public sealed class CameraMovementComponent : Component
{

	enum CameraType
	{
		ThirdPersonSbox
	}
	[Property] public GameObject Body { get; set; }

	[Property] private CameraType type {  get; set; }

	public Angles EyeAngles { get; set; }

	private Transform _initialCamera;

	[Property]
	private Vector3 EyePosition { get; set; }

	[Property]
	private Vector3 CameraPosition { get; set; }



	void ThirdPersonSboxEnabled()
	{
		var cam = Scene.GetAllComponents<CameraComponent>().FirstOrDefault();
		if ( cam is not null )
		{
			Log.Info( "test" );
			var ee = cam.Transform.Rotation.Angles();
			ee.roll = 0;
			EyeAngles = ee;



			var lookDir = EyeAngles.WithPitch( MathX.Clamp( EyeAngles.pitch, -10f, 30f ) ).ToRotation();
			_initialCamera.Position = Transform.Position + lookDir.Backward * 300 + Vector3.Up * 75.0f;
		}
	}

	void ThirdPersonSboxOnUpdate()
	{
		var ee = EyeAngles;
		ee += Input.AnalogLook * 0.5f;
		ee.roll = 0;
		EyeAngles = ee;

		var cam = Scene.GetAllComponents<CameraComponent>().FirstOrDefault();
		var test = _initialCamera.RotateAround( EyePosition, EyeAngles.WithYaw( 0f ) );
		var lookDir = EyeAngles.WithPitch( MathX.Clamp( EyeAngles.pitch, -10f, 30f ) ).ToRotation();
		var ideal = Transform.Position + lookDir.Backward * 150 + Vector3.Up * 75.0f;

		var ray = Scene.Trace.Ray( EyePosition, ideal ).Size( 5f ).IgnoreGameObjectHierarchy( GameObject ).WithoutTags( "player" ).Run();
		testtrace = ray;

		cam.Transform.Rotation = lookDir;
		cam.Transform.Position = ray.EndPosition;

		Transform.LocalRotation = Rotation.FromYaw( lookDir.Yaw() );
	}


	protected override void OnEnabled()
	{
		base.OnEnabled();
		switch ( type )
		{
		   case CameraType.ThirdPersonSbox:
				ThirdPersonSboxEnabled(); 
				break;
		}
	}

	private SceneTraceResult testtrace { get; set; }

	protected override void DrawGizmos()
	{
		if ( Gizmo.IsSelected )
		{
			var draw = Gizmo.Draw;
			draw.LineSphere( EyePosition, 10f );
			draw.LineSphere( CameraPosition, 10f );
			draw.LineCylinder( EyePosition, EyePosition + Vector3.Forward * 50f, 5, 5, 10);
		}
	}

	protected override void OnUpdate()
	{
		switch ( type )
		{
			case CameraType.ThirdPersonSbox:
				ThirdPersonSboxOnUpdate();
				break;
		}
	}
}
