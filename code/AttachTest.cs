using Sandbox;
using Sandbox.Internal;

public sealed class AttachTest : Component, Component.ICollisionListener
{

	//
	// Summary:
	//     Called when this collider/rigidbody starts touching another collider.
	public void OnCollisionStart( Collision other )
	{
		Log.Info( "test" );

		// making dead simple 
		if ( other.Other.GameObject.Components.TryGet<PlayerMovement>( out var player ) )
		{
			Log.Info( "ffff" );

			Transform.Position = Vector3.Zero;
			Log.Info( GameObject.Transform.Position );
			Log.Info( Transform.LocalPosition ); // because returned here
		};
	}

	//
	// Summary:
	//     Called once per physics step for every collider being touched.
	void OnCollisionUpdate( Collision other )
	{
	}

	//
	// Summary:
	//     Called when this collider/rigidbody stops touching another collider.
	void OnCollisionStop( CollisionStop other )
	{
	}

}
