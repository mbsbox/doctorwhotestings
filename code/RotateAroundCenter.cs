using Sandbox;

public sealed class RotateAroundCenter : Component
{
	[Property] private GameObject parent {  get; set; }


	protected override void OnUpdate()
	{

		base.OnUpdate();

		Transform.LocalPosition = Transform.LocalPosition.RotateAround(parent.Transform.LocalPosition, Rotation.FromRoll(1f));
	


	}
}
