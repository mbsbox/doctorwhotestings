using Sandbox;
using Sandbox.Citizen;

public sealed class PlayerMovement : Component
{

	[Property] private CharacterController characterController { get; set; }
	[Property] private CitizenAnimationHelper citizenAnimationHelper { get; set; }
	[Property] private SkinnedModelRenderer skinnedModelRenderer { get; set; }


	[Property] private float WalkSpeed { get; set; } = 120f;
	[Property] private float RunSpeed { get; set; } = 300f;
	[Property] private float JumpForce { get; set; } = 300f;

	public float speed => Input.Down( "run" ) ? RunSpeed : WalkSpeed;

	Vector3 BuildVelocity()
	{
		if ( IsProxy ) return Vector3.Zero;

		var wishVelocity = Vector3.Zero;
		var rot = characterController.Transform.Rotation;

		if ( Input.Down( "forward" ) ) wishVelocity += rot.Forward;
		if ( Input.Down( "backward" ) ) wishVelocity += rot.Backward;
		if ( Input.Down( "left" ) ) wishVelocity += rot.Left;
		if ( Input.Down( "right" ) ) wishVelocity += rot.Right;

		return wishVelocity.Normal * speed;
	}

	[Property] public bool pounched = false;

	protected override void OnStart()
	{
		base.OnStart();

	}


	protected override void OnFixedUpdate()
	{

		base.OnFixedUpdate();

		var wishVelocity = BuildVelocity();
		var gravityVelocity = Scene.PhysicsWorld.Gravity;

		if ( !characterController.IsOnGround )
		{
			characterController.ApplyFriction( 2f );
			characterController.Velocity += gravityVelocity * Time.Delta;
		}
		else
		{
			characterController.ApplyFriction( 5f );

			if ( Input.Pressed( "jump" ) && !IsProxy )
			{
				characterController.Punch( Vector3.Up * JumpForce );
				citizenAnimationHelper.TriggerJump();
			}

		}

		characterController.Accelerate( wishVelocity );
		characterController.Move();

		citizenAnimationHelper.IsGrounded = characterController.IsOnGround;
		citizenAnimationHelper.WithVelocity( characterController.Velocity );


	}
}
